<?php
/**
 * Fichier de la partie d'administration
 * 
 * @file		atix_anonymous_edit.admin.inc
 * @package		Atixnet Anonymous Edit
 * @subpackage	Admin
 * @author		Christophe Crosaz - Atixnet
 */

/**
 * Atixnet Anonymous Edit general settings form.
 * 
 * Allow the settings of the module options
 */
function atix_anonymous_edit_settings() {
	$form		= array();
	$form['general']	= array(
		'#type' => 'fieldset',
		'#title' => t('General settings'),
	);
	$form['general']['intro'] = array(
		'#markup' => '<div>'.t('Define settings for the module and field Atixnet Anonymous Edit.').'</div>',
	);
	$form['general']['anonymous_edit_email_notify'] = array(
		'#type' => 'checkbox',
		'#title' => t('Notify user'),
		'#description' => t('Notify user when content is created.'),
		'#default_value' => variable_get('anonymous_edit_email_notify', TRUE),
	);
	$form['general']['anonymous_edit_admin_notify'] = array(
		'#type' => 'checkbox',
		'#title' => t('Notify administrator'),
		'#description' => t('Notify administrator when content is created.'),
		'#default_value' => variable_get('anonymous_edit_admin_notify', TRUE),
	);
	$form['general']['anonymous_edit_email_anonymous_only'] = array(
		'#type' => 'checkbox',
		'#title' => t('Notify only for anonymous users'),
		'#description' => t('If checked, the messages will be send only if the content was posted by anonymous user.'),
		'#default_value' => variable_get('anonymous_edit_email_anonymous_only', FALSE),
	);
	$form['general']['anonymous_edit_replace_notification'] = array(
		'#type' => 'checkbox',
		'#title' => t('Replace content add notification'),
		'#description' => t('If checked, the status message add by node module will be remove.'),
		'#default_value' => variable_get('anonymous_edit_replace_notification', TRUE),
	);
	
	$email_token_help = t('Available variables are: [site:name], [site:url], [node:title], [node:url], [node:content-type:name], [node:edit_token], [node:delete_token].');
	$form['email_title'] = array(
	  '#type' => 'item',
	  '#title' => t('E-mails'),
	);
	$form['email'] = array(
	  '#type' => 'vertical_tabs',
	  '#group' => 'general',
	);
	
	$form['anonymous_edit_email_token'] = array(
	  '#type' => 'fieldset',
	  '#title' => t('Add content'),
	  '#collapsible' => TRUE,
	  '#collapsed' => FALSE,
	  '#description' => t('Edit the e-mail messages sent to e-mail address when content is created.') . ' ' . $email_token_help,
	  '#group' => 'email',
	);
	$form['anonymous_edit_email_token']['anonymous_edit_email_token_subject'] = array(
		'#type' => 'textfield',
		'#title' => t('Subject'),
		'#default_value' => variable_get('anonymous_edit_email_token_subject', NULL),
		'#maxlength' => 180,
	);
	$form['anonymous_edit_email_token']['anonymous_edit_email_token_body'] = array(
		'#type' => 'textarea',
		'#title' => t('Body'),
		'#default_value' => variable_get('anonymous_edit_email_token_body', NULL),
		'#rows' => 15,
	);
	
	$form['anonymous_edit_email_admin'] = array(
	  '#type' => 'fieldset',
	  '#title' => t('Administrator notification'),
	  '#collapsible' => TRUE,
	  '#collapsed' => FALSE,
	  '#description' => t('Edit the e-mail messages sent to administrator when content is created.') . ' ' . $email_token_help,
	  '#group' => 'email',
	);
	$form['anonymous_edit_email_admin']['anonymous_edit_email_admin_subject'] = array(
		'#type' => 'textfield',
		'#title' => t('Subject'),
		'#default_value' => variable_get('anonymous_edit_email_admin_subject', NULL),
		'#maxlength' => 180,
	);
	$form['anonymous_edit_email_admin']['anonymous_edit_email_admin_body'] = array(
		'#type' => 'textarea',
		'#title' => t('Body'),
		'#default_value' => variable_get('anonymous_edit_email_admin_body', NULL),
		'#rows' => 15,
	);
	
	return system_settings_form($form);
}
