Atixnet Anonymous Edit
====================

This module add a new field type, 'Anonymous Edit Link', that you can add on a content type to manage token buy content for any user to edit or delete it.

It was design to allow anonymous user to be able to edit and delete their own content, by receiving an email with the edition and deletion token links.

This is part of a project on the principle of a craigslist.

Use :
----------

You have to add the 'Anonymous Edit Link' field to each content type you want to allow people to edit anonymously.
The field setting ask you to give the machine name of an E-mail field, from witch the value will be used to send message with the node information and the edition and deletion token links.

This module have a settings page, where you can configure the following mail behaviour :
- Notify user when content is created
- Notify only anonymous user (not those connected)
- Email send when new content is added (Subject and Body)

The email content support native tokens ( [site:name], [site:url], [node:title], [node:url], [node:content-type:name], ... ) and some specific ones ( [node:edit_token], [node:delete_token] ).

When a user (even anonymous) add a content of this type, he receive a mail notification with the links to edit or delete this content. He just have to use those links to manage his own content.

Features :
----------
- Record email address and token for each content
- Send a notification to the email address with content links and informations
- Bypass the edition form access rule on token validation
- Bypass the deletion form access rule on token validation
- Page to get back all the mails with tokens for all the contents where the email address was used
- You can define who can manage the settions (Administer Atixnet Anonymous Edit), and witch role will be able to see the token, email and link informations on form edit and content view.

Notes :
----------

If you need to let anonymous users to add some content, you have to give the role the permission to add this content type. **No need to give the permission to edit or delete the content**.

I did not integrate the email value, and prefer the use of another field, to be able to use some specific field as email (with a contact form) if necessary for the annonces.

Only the native 'node' entity is fully supported for now.	

It's *really not* necessary to add more than one field on each content type.

Possible evolution :
----------

I'm thinking to have a specific mail format for the mail send by the recovery page.

I'm also thinking to integrate the possibility to send a mail to the administrator (or specific email addresses) when some content have been added by anonymous users. I need it for a project, and I think it could be part of this module, instead of developing another one.

7.x-1.x Requirements
------------

This module require the following modules :

- field
- field_sql_storage

Related module(s)
------------

I do not know about other modules proposing the same functionality, it's also why I start developing this one.

But I found another project, call [Node authorize link](https://www.drupal.org/project/node_authlink), witch generates link for view, edit, or delete content without login.

It can be used for anonymous edition and deletion, if you mix it with [rules](https://www.drupal.org/project/rules) to send email to *anonymous owner*.

Sponsors
------------

This module was develope by [Atixnet](http://atixnet.fr)