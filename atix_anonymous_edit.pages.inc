<?php

/**
 * Fichier de la partie frontend
 * 
 * @file		atix_anonymous_edit.pages.inc
 * @package		Atixnet Anonymous Edit
 * @subpackage	Pages
 * @author		Christophe Crosaz - Atixnet
 */

/**
 * Menu callback; presents the object editing form.
 */
function anonedit_page_edit($object_type, $object_id) {
	if ($object_type == 'node') {
		module_load_include('inc', 'node', 'node.pages');
		$node = node_load($object_id);
		return node_page_edit($node);
	}
}

/**
 * Menu callback; presents the object deleting form.
 */
function anonedit_page_delete($object_type, $object_id) {
	if ($object_type == 'node') {
		module_load_include('inc', 'node', 'node.pages');
		$node = node_load($object_id);
		return drupal_get_form('node_delete_confirm', $node);
	}
}

/**
 * Form builder; Request a password reset.
 *
 * @ingroup forms
 * @see anonedit_page_request_validate()
 * @see anonedit_page_request_submit()
 */
function anonedit_page_request() {
	global $user;

	$form['email'] = array(
		'#type' => 'textfield',
		'#title' => t('E-mail address'),
		'#size' => 60,
		'#maxlength' => EMAIL_MAX_LENGTH,
		'#required' => TRUE,
		'#default_value' => isset($_GET['email']) ? $_GET['email'] : '',
	);
	// Allow logged in users to request this also.
	if ($user->uid > 0) {
		$form['email']['#type'] = 'value';
		$form['email']['#value'] = $user->mail;
		$form['mail'] = array(
			'#prefix' => '<p>',
			'#markup' => t('Edition tokens will be mailed to %email.', array('%email' => $user->mail)),
			'#suffix' => '</p>',
		);
	}
	$form['actions'] = array('#type' => 'actions');
	$form['actions']['submit'] = array('#type' => 'submit', '#value' => t('E-mail edition tokens'));

	return $form;
}

function anonedit_page_request_validate($form, &$form_state) {
	$email = trim($form_state['values']['email']);
	
	// Verify email address is valide
	if (!valid_email_address($email)) {
		form_set_error('email', t('The e-mail address %mail is not valid.', array('%mail' => $email)));
	} else {
		// Get the entities related to this email address by fields
		$fields	= anonedit_load_fields_from_mail($email);
		
		if (count($fields)) {
			form_set_value(array('#parents' => array('anonedit_fields')), $fields, $form_state);
		} else {
			form_set_error('email', t('Sorry, %mail is not recognized as edition e-mail address.', array('%mail' => $email)));
		}
	}
}

function anonedit_page_request_submit($form, &$form_state) {
	global $language;

	$fields		= $form_state['values']['anonedit_fields'];
	
	foreach ($fields as $fname => $fvalues) {
		foreach ($fvalues as $entity_type => $ids) {
			$entities		= entity_load($entity_type, $ids);
			
			foreach ($entities as $entity) {
				$field_language = field_language($entity_type, $entity, $fname);
				
				$items			= $entity->{$fname}[$field_language];

				if (anonedit_send_mail($entity_type, $entity, $items)) {
					drupal_set_message(t('A message with the edition token has been sent to your e-mail address.'));
				}
			}
		}
	}

//	$form_state['redirect'] = 'user';
	return;
}
